<?php

/**
 * @file
 * uw_ct_important_date.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_important_date_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_uw_important_dates_tags|node|uw_important_date|form';
  $field_group->group_name = 'group_uw_important_dates_tags';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_important_date';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Taxonomies',
    'weight' => '3',
    'children' => array(
      0 => 'field_audience',
      1 => 'field_important_date_term',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-uw-important-dates-tags field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_uw_important_dates_tags|node|uw_important_date|form'] = $field_group;

  return $export;
}
