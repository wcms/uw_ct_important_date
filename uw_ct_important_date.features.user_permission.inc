<?php
/**
 * @file
 * uw_ct_important_date.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_important_date_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_important_date content'.
  $permissions['create uw_important_date content'] = array(
    'name' => 'create uw_important_date content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_important_date content'.
  $permissions['delete any uw_important_date content'] = array(
    'name' => 'delete any uw_important_date content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_important_date content'.
  $permissions['delete own uw_important_date content'] = array(
    'name' => 'delete own uw_important_date content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_important_date content'.
  $permissions['edit any uw_important_date content'] = array(
    'name' => 'edit any uw_important_date content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_important_date content'.
  $permissions['edit own uw_important_date content'] = array(
    'name' => 'edit own uw_important_date content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'override uw_important_date authored by option'.
  $permissions['override uw_important_date authored by option'] = array(
    'name' => 'override uw_important_date authored by option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_important_date authored on option'.
  $permissions['override uw_important_date authored on option'] = array(
    'name' => 'override uw_important_date authored on option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_important_date promote to front page option'.
  $permissions['override uw_important_date promote to front page option'] = array(
    'name' => 'override uw_important_date promote to front page option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_important_date published option'.
  $permissions['override uw_important_date published option'] = array(
    'name' => 'override uw_important_date published option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_important_date revision option'.
  $permissions['override uw_important_date revision option'] = array(
    'name' => 'override uw_important_date revision option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_important_date sticky option'.
  $permissions['override uw_important_date sticky option'] = array(
    'name' => 'override uw_important_date sticky option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  return $permissions;
}
