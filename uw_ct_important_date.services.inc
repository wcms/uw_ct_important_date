<?php

/**
 * @file
 * uw_ct_important_date.services.inc
 */

/**
 * Implements hook_default_services_endpoint().
 */
function uw_ct_important_date_default_services_endpoint() {
  $export = array();

  $endpoint = new stdClass();
  $endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
  $endpoint->api_version = 3;
  $endpoint->name = 'uwaterloo_important_dates_v1';
  $endpoint->server = 'rest_server';
  $endpoint->path = 'api/v1/important-dates';
  $endpoint->authentication = array(
    'services_api_key_auth' => array(
      'api_key' => '96ab9383e6ad48c23aa1504dc9cc5c52',
      'user' => '4',
    ),
  );
  $endpoint->server_settings = array();
  $endpoint->resources = array(
    'all_important_dates' => array(
      'operations' => array(
        'index' => array(
          'enabled' => '1',
        ),
      ),
    ),
  );
  $endpoint->debug = 0;
  $export['uwaterloo_important_dates_v1'] = $endpoint;

  return $export;
}
