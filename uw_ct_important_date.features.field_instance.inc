<?php
/**
 * @file
 * uw_ct_important_date.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function uw_ct_important_date_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-uw_important_date-field_audience'.
  $field_instances['node-uw_important_date-field_audience'] = array(
    'bundle' => 'uw_important_date',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 3,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_audience',
    'label' => 'Audience',
    'required' => 0,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'term_reference_tree',
      'settings' => array(
        'cascading_selection' => 1,
        'filter_view' => '',
        'label_help_description' => '',
        'leaves_only' => 0,
        'max_depth' => '',
        'parent_term_id' => '',
        'select_parents' => 0,
        'start_minimized' => 0,
        'token_display' => '',
        'track_list' => 0,
      ),
      'type' => 'term_reference_tree',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'node-uw_important_date-field_body_no_summary'.
  $field_instances['node-uw_important_date-field_body_no_summary'] = array(
    'bundle' => 'uw_important_date',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_body_no_summary',
    'label' => 'Description',
    'required' => 0,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'full_html' => 'full_html',
          'plain_text' => 0,
          'uw_tf_basic' => 0,
          'uw_tf_comment' => 0,
          'uw_tf_contact' => 0,
          'uw_tf_standard' => 'uw_tf_standard',
          'uw_tf_standard_sidebar' => 0,
          'uw_tf_standard_site_footer' => 0,
          'uw_tf_standard_wide' => 0,
        ),
        'allowed_formats_toggle' => 1,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'full_html' => array(
              'weight' => -6,
            ),
            'plain_text' => array(
              'weight' => 10,
            ),
            'uw_tf_basic' => array(
              'weight' => 0,
            ),
            'uw_tf_comment' => array(
              'weight' => 0,
            ),
            'uw_tf_contact' => array(
              'weight' => -5,
            ),
            'uw_tf_standard' => array(
              'weight' => -10,
            ),
            'uw_tf_standard_sidebar' => array(
              'weight' => -8,
            ),
            'uw_tf_standard_site_footer' => array(
              'weight' => -7,
            ),
            'uw_tf_standard_wide' => array(
              'weight' => -9,
            ),
          ),
        ),
      ),
      'entity_translation_sync' => FALSE,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'label_help_description' => '',
        'rows' => 20,
      ),
      'type' => 'text_textarea',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-uw_important_date-field_important_date'.
  $field_instances['node-uw_important_date-field_important_date'] = array(
    'bundle' => 'uw_important_date',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
          'show_remaining_days' => FALSE,
          'show_repeat_rule' => 'show',
        ),
        'type' => 'date_default',
        'weight' => 1,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_important_date',
    'label' => 'Date and time',
    'required' => 1,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'blank',
      'default_value_code' => '',
      'default_value_code2' => '',
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'display_all_day' => 1,
        'increment' => 1,
        'input_format' => 'M j Y - g:i:sa',
        'input_format_custom' => '',
        'label_help_description' => '',
        'label_position' => 'above',
        'repeat_collapsed' => 1,
        'text_parts' => array(),
        'year_range' => '-1:+3',
      ),
      'type' => 'date_popup',
      'weight' => 1,
    ),
  );

  // Exported field_instance:
  // 'node-uw_important_date-field_important_date_term'.
  $field_instances['node-uw_important_date-field_important_date_term'] = array(
    'bundle' => 'uw_important_date',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 4,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_important_date_term',
    'label' => 'Term',
    'required' => 1,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'cascading_selection' => 0,
        'filter_view' => '',
        'label_help_description' => '',
        'leaves_only' => 0,
        'max_depth' => '',
        'parent_term_id' => '',
        'select_parents' => 0,
        'start_minimized' => 0,
        'token_display' => '',
        'track_list' => 0,
      ),
      'type' => 'options_select',
      'weight' => 8,
    ),
  );

  // Exported field_instance:
  // 'taxonomy_term-important_date_terms-field_term_date_range'.
  $field_instances['taxonomy_term-important_date_terms-field_term_date_range'] = array(
    'bundle' => 'important_date_terms',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_term_date_range',
    'label' => 'Date range',
    'required' => 0,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'label_help_description' => '',
      ),
      'type' => 'options_select',
      'weight' => 1,
    ),
  );

  // Exported field_instance:
  // 'taxonomy_term-important_date_terms-field_term_id'.
  $field_instances['taxonomy_term-important_date_terms-field_term_id'] = array(
    'bundle' => 'important_date_terms',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_term_id',
    'label' => 'Term ID',
    'required' => 0,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'full_html' => 'full_html',
          'plain_text' => 'plain_text',
          'uw_tf_basic' => 'uw_tf_basic',
          'uw_tf_comment' => 'uw_tf_comment',
          'uw_tf_contact' => 'uw_tf_contact',
          'uw_tf_standard' => 'uw_tf_standard',
          'uw_tf_standard_sidebar' => 'uw_tf_standard_sidebar',
          'uw_tf_standard_site_footer' => 'uw_tf_standard_site_footer',
          'uw_tf_standard_wide' => 'uw_tf_standard_wide',
        ),
        'allowed_formats_toggle' => 0,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'full_html' => array(
              'weight' => -6,
            ),
            'plain_text' => array(
              'weight' => 10,
            ),
            'uw_tf_basic' => array(
              'weight' => 0,
            ),
            'uw_tf_comment' => array(
              'weight' => 0,
            ),
            'uw_tf_contact' => array(
              'weight' => -5,
            ),
            'uw_tf_standard' => array(
              'weight' => -10,
            ),
            'uw_tf_standard_sidebar' => array(
              'weight' => -8,
            ),
            'uw_tf_standard_site_footer' => array(
              'weight' => -7,
            ),
            'uw_tf_standard_wide' => array(
              'weight' => -9,
            ),
          ),
        ),
      ),
      'entity_translation_sync' => FALSE,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'label_help_description' => '',
        'size' => 4,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Audience');
  t('Date and time');
  t('Date range');
  t('Description');
  t('Term');
  t('Term ID');

  return $field_instances;
}
