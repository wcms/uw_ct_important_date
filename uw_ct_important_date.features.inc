<?php

/**
 * @file
 * uw_ct_important_date.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_important_date_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "services" && $api == "services") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_important_date_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_important_date_node_info() {
  $items = array(
    'uw_important_date' => array(
      'name' => t('Important Date'),
      'base' => 'node_content',
      'description' => t('An important date relevant to students, staff and faculty.'),
      'has_title' => '1',
      'title_label' => t('Important Date TItle'),
      'help' => '',
    ),
  );
  return $items;
}
